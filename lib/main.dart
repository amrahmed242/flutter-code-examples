import 'package:flutter/material.dart';
import 'package:testapp/code/example1.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'code examples',
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.lightGreen, //button's fill color
        centerTitle: true,
        title: Text('Code examples👀👏🍉'),
      ),
      body: Center(
          child: Container(
        width: 300,
        child: ListView(
          children: buildExpansionTiles(context),
          shrinkWrap: true,
        ),
      )),
    );
  }

  Widget exampleButton(BuildContext context, String exampleTitle, Widget codePage) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.lightGreen, //button's fill color
          onPrimary: Colors
              .yellow, //specify the color of the button's text and icons as well as the overlay colors used to indicate the hover, focus, and pressed states
          onSurface: Colors.orange, //specify the button's disabled text, icon, and fill color
          shadowColor: Colors.black, //specify the button's elevation color
          elevation: 7.0, //buttons Material shadow
          textStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), //specify the button's text TextStyle
          padding: const EdgeInsets.all(30), //specify the button's Padding
          minimumSize: Size(20, 40), //specify the button's first: width and second: height

          // side: BorderSide(color: Colors.yellow, width: 2.0, style: BorderStyle.solid), //set border for the button
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)), // set the buttons shape. Make its birders rounded etc
          enabledMouseCursor: MouseCursor.defer, //used to construct ButtonStyle.mouseCursor
          disabledMouseCursor: MouseCursor.uncontrolled, //used to construct ButtonStyle.mouseCursor
          visualDensity: VisualDensity(horizontal: 0.0, vertical: 0.0), //set the button's visual density
          tapTargetSize:
              MaterialTapTargetSize.padded, // set the MaterialTapTarget size. can set to: values, padded and shrinkWrap properties
          animationDuration: Duration(milliseconds: 100), //the buttons animations duration
          enableFeedback: true, //to set the feedback to true or false
          alignment: Alignment.bottomCenter, //set the button's child Alignment
        ),
        child: Text(exampleTitle),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (c) {
            return codePage;
          }));
        },
      ),
    );
  }

  Widget expansionTile(BuildContext context, String tileTitle, List<Widget> codes) {
    return ExpansionTile(
      iconColor: Colors.white,
      collapsedIconColor: Colors.white,
      title: Text(
        tileTitle,
        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, color: Colors.white),
      ),
      children: <Widget>[
        Column(children: codes),
      ],
    );
  }

  List<Widget> buildExpansionTiles(BuildContext context) {
    return [
      expansionTile(
        context,
        'animations',
        [
          exampleButton(context, 'exampleTitle', Example1()),
        ],
      ),
      expansionTile(
        context,
        'hocks',
        [
          exampleButton(context, 'exampleTitle', Example1()),
        ],
      ),
    ];
  }
}
