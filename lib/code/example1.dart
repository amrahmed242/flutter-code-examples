import 'package:flutter/material.dart';

class Example1 extends StatelessWidget {
  const Example1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.lightGreen, //button's fill color
        centerTitle: true,
        title: Text('Code examples👀👏🍉'),
      ),
      body: Center(
          child: Container(
        width: 300,
        child: ListView(
          children: [],
          shrinkWrap: true,
        ),
      )),
    );
  }
}
